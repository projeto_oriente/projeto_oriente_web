# Generated by Django 2.1.1 on 2019-01-12 04:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('documentos', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='documento',
            name='titulo',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
