from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.viewsets import ModelViewSet

from .models import Documento, Recomendacao
from .serializers import RecomendacaoSerializerExpandido, DocumentoSerializerSimples


class DocumentoViewSet(ModelViewSet):
    queryset = Documento.objects.all()
    serializer_class = DocumentoSerializerSimples
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)

    def get_queryset(self):
        queryset = super().get_queryset()
        sync = self.request.query_params.get('sync', None)

        if sync:
            if sync == "False":
                sync = False
            else:
                sync = True
            queryset = queryset.filter(sync=sync)

        return queryset

    def list(self, request):
        url = request.query_params.get("url", None)
        if url is not None:
            self.kwargs["url"] = url
            self.lookup_field = "url"
            return self.retrieve(request, pk=url)
        else:
            return super().list(request)

    @csrf_exempt
    def create(self, request):
        return super().create(request)


class RecomendacaoViewSet(ModelViewSet):
    queryset = Recomendacao.objects.all()
    serializer_class = RecomendacaoSerializerExpandido
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)

    def get_queryset(self):
        queryset = super().get_queryset()
        documento_origem_param = self.request.query_params.get("documento_origem", None)
        if documento_origem_param is not None:
            try:
                documento = Documento.get_by_url(documento_origem_param)
            except Documento.DoesNotExist:
                documento = None
            queryset = queryset.filter(documento_origem=documento)

        return queryset
