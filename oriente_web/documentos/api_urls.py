from django.urls import path
from rest_framework.routers import DefaultRouter

from .api_views import (
    DocumentoViewSet,
    RecomendacaoViewSet,
)

app_name = "api_documentos"

router = DefaultRouter()
router.register(r'recomendacoes', RecomendacaoViewSet)
router.register(r'documentos', DocumentoViewSet)

urlpatterns = router.urls