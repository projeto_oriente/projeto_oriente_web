from django.http import Http404
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (
    DetailView,
    FormView
)

from .forms import DocumentoPesquisaForm
from .models import Documento, Recomendacao


class DocumentoPesquisaFormView(FormView):
    form_class = DocumentoPesquisaForm
    template_name = "documentos/documento_pesquisa_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["total_documentos"] = Documento.objects.all().count()
        context["total_recomendacoes"] = Recomendacao.objects.all().count()
        return context

    def form_valid(self, form):
        get_url = form.cleaned_data["url"]
        
        try:
            documento = Documento.get_by_url(a_url=get_url)
        except Documento.DoesNotExist:
            raise Http404

        self.success_url = reverse_lazy("documentos:documento_detail", kwargs={"pk": documento.pk})
        return super().form_valid(form)


class DocumentoDetailView(DetailView):
    model = Documento
    template_name = "documentos/documento_detail.html"


def documento_ajax_detail(request):
    template_name = "documentos/documento_ajax_detail.html"
    context = {}

    get_url = request.GET.get("url", None)

    if get_url is not None:
        try:
            documento = Documento.documento = Documento.get_by_url(a_url=get_url)
            context["documento"] = documento
        except Documento.DoesNotExist:
            pass

    return render(request, template_name, context)


def quantidade_documentos_repositorios(request):
    template_name = 'documentos/quantidade_documentos_repositorios.html'
    context = {}
    repositorios = ['repositorio.uft.edu.br', 'acervodigital.ufpr.br', 'sigmauft.pythonanywhere.com',
                    'repositorio.bc.ufg.br']

    context['repositorios'] = {}
    for repositorio in repositorios:
        context['repositorios'][repositorio] = Documento.objects.filter(url__icontains=repositorio).count()

    return render(request, template_name, context)
