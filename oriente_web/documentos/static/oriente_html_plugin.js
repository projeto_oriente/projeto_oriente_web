
function recomendacao_to_a(recomendacao){
    var a = document.createElement("a");
    a.textContent = recomendacao.documento_destino_titulo;
    a.setAttribute("href", recomendacao.documento_destino_url);
    return a;
}

function recomendacao_to_p(recomendacao) {
    var p = document.createElement("p");
    p.appendChild(recomendacao_to_a(recomendacao));
    return p;
}

function load_recomendacoes(div_id, endpoint, url_documento) {
    var tag = document.getElementById(div_id);

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200)
        {
            tag.innerHTML = "";
            json = JSON.parse(this.response);
            json['results'].forEach(function (obj) {
                tag.appendChild(recomendacao_to_p(obj));
            });
        } else {
            tag.innerHTML = "carregando recomendações...";
        }
    }
    var request_url = endpoint + "/recomendacoes/?documento_origem="  + url_documento;

    xhttp.open("get", request_url, true);
    xhttp.send();
}
