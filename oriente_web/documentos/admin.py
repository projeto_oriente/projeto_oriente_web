from django.contrib import admin

from .models import Documento, Recomendacao


class TimeStampedAdminMixin(object):
    def get_list_display(self, request):
        list_display = super().get_list_display(request)
        return list_display + ("criado_em", "editado_em")


class DocumentoAdmin(TimeStampedAdminMixin, admin.ModelAdmin):
    list_display = ("id", "url", "url_arquivo")
    list_filter = ("sync",)
    actions = ('mark_synced', )

    def mark_synced(self, request, queryset):
        queryset.update(sync=True)

    mark_synced.short_description = "Mark selected documentos as synced"


admin.site.register(Documento, DocumentoAdmin)


class RecomendacaoAdmin(TimeStampedAdminMixin, admin.ModelAdmin):
    list_display = ("id", "documento_origem", "documento_destino", "fator")


admin.site.register(Recomendacao, RecomendacaoAdmin)
