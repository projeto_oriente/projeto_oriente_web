from urllib.parse import urlparse, ParseResult

from django.db import models


def toggle_http_https(url):
    u = urlparse(url)

    method = "http"
    if u[0] == "http":
        method = "https"

    return ParseResult(method, *u[1:]).geturl()


class UrlNotStr(Exception):
    pass


class TimeStempedMixin(models.Model):
    criado_em = models.DateTimeField(auto_now_add=True)
    editado_em = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Documento(TimeStempedMixin):
    """Documento
    Modelo que representa um documento do corpus
    url: endereço onde pode-se encontrar informações sobre o documento
    titulo: titulo do documento
    url_arquivo: endereço onde pode-se encontrar o documento completo
    """
    url = models.URLField(unique=True)
    titulo = models.CharField(max_length=255)
    url_arquivo = models.URLField(unique=True)
    sync = models.BooleanField()

    objects = models.Manager()

    def __str__(self):
        return self.url

    @classmethod
    def get_by_url(cls, a_url=None):
        """
        Get documento por url independente de http ou https
        :param a_url: [http:https]://url
        :return: Documento
        """
        assert type(a_url) is str, "Url não é string"

        b_url = toggle_http_https(a_url)
        a_query = models.Q(url=a_url)
        b_query = models.Q(url=b_url)
        return cls.objects.get(a_query | b_query)


class Recomendacao(TimeStempedMixin):
    """Recomendacao
    Modelo que relaciona dois documentos destintos com um fator
    A relação é simetrica, para cara relação existe origem para
    os dois Documentos
    """
    documento_origem = models.ForeignKey(Documento, on_delete=models.CASCADE, related_name="recomendacoes")
    documento_destino = models.ForeignKey(Documento, on_delete=models.CASCADE, related_name="recomendacoes_destino")
    fator = models.FloatField()

    objects = models.Manager()

    class Meta:
        ordering = ("-fator",)
        verbose_name = "Recomendação"
        verbose_name_plural = "Recomendações"
        unique_together = ("documento_origem", "documento_destino")

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        queryset = Recomendacao.objects.filter(
            documento_origem=self.documento_destino,
            documento_destino=self.documento_origem,
        )
        if queryset.count() < 1:
            Recomendacao.objects.create(
                documento_origem=self.documento_destino,
                documento_destino=self.documento_origem,
                fator=self.fator,
            )
