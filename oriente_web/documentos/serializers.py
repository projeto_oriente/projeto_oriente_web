from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import Documento, Recomendacao


class DocumentoSerializerSimples(ModelSerializer):
    class Meta:
        model = Documento
        fields = (
            "id",
            "url",
            "titulo",
            "sync",
            "url_arquivo",
            "criado_em",
            "editado_em",
        )


class DocumentoSerializerParaRecomendacao(ModelSerializer):
    class Meta:
        model = Documento
        fields = (
            "id",
            "url",
            "titulo",
        )


class RecomendacaoSerializerExpandido(ModelSerializer):
    documento_destino_titulo = SerializerMethodField()
    documento_destino_url = SerializerMethodField()

    class Meta:
        model = Recomendacao
        fields = (
            "id",
            "documento_origem",
            "documento_destino",
            "documento_destino_url",
            "documento_destino_titulo",
            "fator",
            "criado_em", "editado_em",
        )

    def get_documento_destino_url(self, obj):
        return obj.documento_destino.url

    def get_documento_destino_titulo(self, obj):
        return obj.documento_destino.titulo


class RecomendacaoSerializerSimples(ModelSerializer):
    documento_destino = DocumentoSerializerSimples()

    class Meta:
        model = Recomendacao
        fields = ("documento_destino", "fator")


class DocumentoSerializer(ModelSerializer):
    recomendacoes = RecomendacaoSerializerSimples(many=True, read_only=True)

    class Meta:
        model = Documento
        fields = (
            "id",
            "url",
            "titulo",
            "sync",
            "url_arquivo",
            "criado_em",
            "editado_em",

            "recomendacoes",
        )
