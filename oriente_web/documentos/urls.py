from django.urls import path, include
from . import views

app_name = "documentos"

urlpatterns = [
    path("", views.DocumentoPesquisaFormView.as_view(), name = "documento_pesquisa_form"),
    path("documento/<int:pk>/", views.DocumentoDetailView.as_view(), name = "documento_detail"),
    path("documento_ajax_detail/", views.documento_ajax_detail, name = "documento_ajax_detail"),
    path('repositorios/', views.quantidade_documentos_repositorios, name='repositorios'),
]
