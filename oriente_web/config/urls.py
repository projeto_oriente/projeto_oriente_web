from django.contrib import admin
from django.urls import path, include

from documentos.views import DocumentoPesquisaFormView as IndexView

from rest_framework.schemas import get_schema_view

schema_view = get_schema_view(title="Oriente Web")

urlpatterns = [
    path('', IndexView.as_view(), name = "index"),
    path('admin/', admin.site.urls),
    path('documentos/', include('documentos.urls')),
    path('api/', include('documentos.api_urls')),
    path('api/schema', schema_view),
]
